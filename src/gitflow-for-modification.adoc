= Gitflow for modification

We are going to modify a file in order to simplify its complexity. We are going through few recommandations to follow in order to solve the problem.

<<<

== File to modify

* Open eclipse
* Open windows explorer into "mas-gin-entretienprofessionnelformation\service\src"
* Right click and open Git Bash
* Enter the following commands : 
[source, git bash]
----
git init
git add .
git commit -m "Initial commit"
----

* Find AgentCampagneDomaineImpl.java in mas-gin project
* Locate method called _modifierTypeCR_

Try to simplify it by following those rules

* Create Junit test case *AgentCampagneDomaineImplTest.java* inside src/test directory with the same path as it has in src/main directory.

* Write a method called testModifierTypeCR();

* Mock all its collaborator

* Instanciate AgentCampagneDomaineImpl

* Try to make relevant assertions

[IMPORTANT]
=====
It will be very difficult to find relevant assertion because this method is not SOLID.

=====

* So normally you should find a way to test it before modifying anything inside production code. But for sanity purpose we are going to break this rule with the help of Git an Junit.

* Find class collaborator and put them as parameter of a private method whith the same name.

.First step
[source,java]
-----
@Override
	public void modifierTypeCR(final Map < Long, Long > idsAvecTypeCR) throws FonctionnelleException {

		modifierTypeCR(idsAvecTypeCR, agentCampagnePersistance [...]);

	}

----- 

* Copy/paste the old content into the new method

* Once you have done copy/paste, commit it.

* Write private boolean methods with a meaningfull name instead of comparison like this :

[source,java]
-----
if(typeCompteRenduManuel != null && typeCompteRenduManuel.getIdTypeCompteRendu() != null)
-----

* Write other usefull methods that you can find in order to alleviate the method.

* Make the list "typesCompteRenduPar" closer to the code where it is usefull

* Find by your own others things to do. If you have no idea you can follow those following clues :

** Extract lists "AgentsCampagnesAModifier" and "IdAgentAvecCrASupprimer" by using them as singleton.

** Look at variables'name : are they always meaningful ?

** Find a way to have less parameters for methods with more than 3 parameters.


